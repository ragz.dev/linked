import { useMutation } from "@apollo/client";
import { useRouter } from "next/router";
import { LOGOUT } from "../../apollo/mutations";
import { cleanUp } from "../../services/auth";
import Button from "../button/button";
import CenteredLayout from "../centered-layout/centered-layout";

const Header: React.FC = () => {
  const router = useRouter();
  const [logout, { client, error, data }] = useMutation<{ logout: any }>(
    LOGOUT
  );

  const logoutHandler = async () => {
    try {
      await logout();
      cleanUp();
      router.push("/");
    } catch (logoutError) {
      console.error(logoutError);
    }
  };

  return (
    <header>
      <CenteredLayout className="flex w-full justify-end p-4">
        <Button type="button" onClick={logoutHandler}>
          logout
        </Button>
      </CenteredLayout>
    </header>
  );
};

export default Header;
