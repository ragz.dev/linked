import { styled } from "@twind/react";

const Button = styled("button", {
  base: `
    appearance-none focus:ring-4 focus:ring-blue-300 disabled:opacity-50
    hover:shadow-sm focus:shadow-sm rounded transition-all
  `,

  variants: {
    size: {
      sm: `text-sm py-0 px-1`,
      md: `text-base py-2 px-3`,
      lg: `text-lg py-3 px-4`,
    },

    color: {
      dark: `bg-dark text-white`,
    },

    fill: {
      outline: `bg-transparent ring-1`,
    },

    flexGrow: {
      1: "flex-grow",
    },
  },

  defaults: {
    color: "dark",
    size: "md",
  },
});

export default Button;

// import { styled as twStyled } from "@twind/react";

// const Button = twStyled("button", {
//   base: `
//     transition-all appearance-none border-none bg-transparent
//     rounded focus:outline-none focus:ring focus:ring-blue-300
//   `,

//   variants: {
//     size: {
//       sm: `text-sm py-0 px-1`,
//       md: `text-base h-9 py-2 px-3`,
//       lg: `text-lg py-3 px-4`,
//     },

//     variant: {
//       dark: `
//       text-white bg-dark
//       `,
//     },

//     outlined: {
//       true: `bg-transparent ring-1`,
//     },
//   },

//   defaults: {
//     variant: "dark",
//     size: "md",
//   },
// });

// export default Button;
