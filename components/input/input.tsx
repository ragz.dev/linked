import { forwardRef } from "react";
import { tw } from "twind";

const Input: React.FC<React.HTMLProps<HTMLInputElement>> = forwardRef(
  ({ className = "", ...rest }, ref) => (
    <input
      ref={ref}
      className={tw`bg-white rounded border border-gray-300 focus:border-dark 
    focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8
    transition-colors duration-200 ease-in-out w-full ${className}`}
      {...rest}
    />
  )
);

Input.propTypes = {};

export default Input;
