import React from "react";
import { tw } from "twind";

const CenteredLayout: React.FC<any> = ({ children, className, ...rest }) => (
  <div className={tw`md:container lg:max-w-5xl mx-auto p-4 ${className}`}>
    {children}
  </div>
);

export default CenteredLayout;
