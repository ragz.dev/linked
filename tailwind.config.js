const twindConfig = require("./twind.config");

module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  ...twindConfig,
  variants: {
    extend: {
      opacity: ["disabled"],
    },
  },
  plugins: [],
};
