/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useMutation, useQuery } from "@apollo/client";
import Bookmark from "@bit/feathericons.react-feather.bookmark";
import RefreshCw from "@bit/feathericons.react-feather.refresh-cw";
import Clipboard from "@bit/feathericons.react-feather.clipboard";
import LinkIcon from "@bit/feathericons.react-feather.link";
import Share from "@bit/feathericons.react-feather.share-2";
import Trash from "@bit/feathericons.react-feather.trash-2";
import { tw } from "twind";
import useDebounce from "../hooks/use-debounce";
import Header from "../components/header/header";
import CenteredLayout from "../components/centered-layout/centered-layout";
import { FIND_ACCOUNT_BY_ID } from "../apollo/queries";
import { getUser, isLoggedIn } from "../services/auth";
import Input from "../components/input/input";
import { CREATE_BOOKMARK, UPDATE_TMP } from "../apollo/mutations";
import Button from "../components/button/button";

export const Home: React.FC = () => {
  const router = useRouter();
  const [linkText, setLinkText] = useState("");
  const [userId, setUserId] = useState("");
  const debouncedSearchTerm = useDebounce(linkText, 300);

  const { loading, error, data, refetch } = useQuery<{ findAccountByID: any }>(
    FIND_ACCOUNT_BY_ID,
    {
      variables: { id: userId },
    }
  );

  const [
    createBookmark,
    {
      loading: createBookmarkLoading,
      error: createBookmarkError,
      data: createBookmarkData,
    },
  ] = useMutation<{ createBookmark: any }>(CREATE_BOOKMARK, {
    refetchQueries: [{ query: FIND_ACCOUNT_BY_ID, variables: { id: userId } }],
  });

  const [
    updateTmp,
    { loading: updateTmpLoading, error: updateTmpError, data: updateTmpData },
  ] = useMutation<{ updateTmp: any }>(UPDATE_TMP, {
    refetchQueries: [{ query: FIND_ACCOUNT_BY_ID, variables: { id: userId } }],
  });

  const copyToClipboard = async (link) => {
    if (navigator.clipboard) {
      try {
        await navigator.clipboard.writeText(link);
      } catch (err) {
        console.error(err);
      }
    }
  };

  useEffect(() => {
    if (!isLoggedIn()) {
      router.push("/");
    }
    const { _id: id } = getUser();
    setUserId(id);
  }, []);

  const saveLink = async () => {
    createBookmark({
      variables: {
        link: linkText,
        profileID: data?.findAccountByID.profile._id,
      },
    });
  };

  useEffect(() => {
    if (!debouncedSearchTerm) {
      return;
    }
    const updateNewTmp = () => {
      updateTmp({
        variables: {
          link: debouncedSearchTerm,
          id: data?.findAccountByID.profile.tmp._id,
        },
      });
    };
    updateNewTmp();
  }, [debouncedSearchTerm]);

  useEffect(() => {
    if (data?.findAccountByID) {
      setLinkText(data.findAccountByID.profile.tmp.link);
    }
  }, [data]);

  return (
    <>
      <Header />
      <CenteredLayout className="p-4 space-y-4">
        <div className={tw`flex space-x-4`}>
          <Input
            placeholder="insert link to share"
            onChange={(e) => setLinkText((e.target as any).value)}
            value={linkText}
          />
          <Button
            type="button"
            onClick={() => {
              refetch();
            }}
          >
            <RefreshCw color="#fff" />
          </Button>
        </div>
        <div className={tw`flex space-x-4`}>
          <Button type="button" onClick={saveLink}>
            <Bookmark />
          </Button>
          <Button onClick={() => copyToClipboard(linkText)}>
            <Clipboard color="#fff" />
          </Button>
          <a href={linkText} rel="noopener noreferrer nofollow" target="_blank">
            <Button>
              <LinkIcon color="#fff" />
            </Button>
          </a>
          <Button type="button" onClick={saveLink}>
            <Share />
          </Button>
        </div>
      </CenteredLayout>
      <CenteredLayout className={tw`space-y-4`}>
        {data?.findAccountByID &&
          data.findAccountByID.profile.bookmarks.data.map(({ link, _id }) => (
            <div
              className={tw`border border-grey-300 rounded-sm p-4 flex items-center relative`}
              key={_id}
            >
              <a
                className={tw`underline w-full`}
                href={link}
                rel="noopener noreferrer nofollow"
                target="_blank"
              >
                {link}
              </a>

              <div className={tw`space-x-4 flex-shrink-0`}>
                <Button onClick={link}>
                  <Bookmark />
                </Button>
                <Button onClick={() => copyToClipboard(linkText)}>
                  <Clipboard color="#fff" />
                </Button>
                <a
                  href={linkText}
                  rel="noopener noreferrer nofollow"
                  target="_blank"
                >
                  <Button>
                    <LinkIcon color="#fff" />
                  </Button>
                </a>
                <Button onClick={saveLink}>
                  <Share />
                </Button>
              </div>

              <Button
                className={tw`absolute right-0 top-0 transform -translate-y-2/4 translate-x-2/4 px-0`}
              >
                <Trash size={16} />
              </Button>
            </div>
          ))}
      </CenteredLayout>
    </>
  );
};

export default Home;
