import { useMutation, useQuery } from "@apollo/client";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import Trash2 from "@bit/feathericons.react-feather.trash-2";
import RefreshCw from "@bit/feathericons.react-feather.refresh-cw";
import Clipboard from "@bit/feathericons.react-feather.clipboard";
import LinkIcon from "@bit/feathericons.react-feather.link";
import { tw } from "twind";
import { FIND_ANONYMOUS_TMP_BY_SHORTID } from "../../apollo/queries";
import Button from "../../components/button/button";
import CenteredLayout from "../../components/centered-layout/centered-layout";
import Input from "../../components/input/input";
import useDebounce from "../../hooks/use-debounce";
import "react-toastify/dist/ReactToastify.css";
import {
  DELETE_ANONYMOUS_TMP,
  UPDATE_ANONYMOUS_TMP,
} from "../../apollo/mutations";

const Anonymous: React.FC = () => {
  const router = useRouter();
  const { shortId } = router.query;
  const [linkText, setLinkText] = useState("");
  const debouncedSearchTerm = useDebounce(linkText, 300);

  const { loading, error, data, refetch } = useQuery<{
    findAnonymousTmpByShortId: any;
  }>(FIND_ANONYMOUS_TMP_BY_SHORTID, {
    variables: { shortId },
  });

  const [deleteSession, { loading: deleteLoading }] = useMutation(
    DELETE_ANONYMOUS_TMP
  );

  const [
    updateTmp,
    { loading: updateTmpLoading, error: updateTmpError, data: updateTmpData },
  ] = useMutation<{ updateTmp: any }>(UPDATE_ANONYMOUS_TMP, {
    refetchQueries: [
      {
        query: FIND_ANONYMOUS_TMP_BY_SHORTID,
        variables: { shortId },
      },
    ],
  });

  const deleteSessionHandler = async () => {
    try {
      await deleteSession({
        variables: { id: data?.findAnonymousTmpByShortId._id },
      });
      router.push("/");
    } catch (deleteSessionError) {
      console.error(deleteSessionError);
    }
  };

  if (error || (data && !data.findAnonymousTmpByShortId)) {
    router.push("/");
  }

  useEffect(() => {
    if (data && data.findAnonymousTmpByShortId) {
      setLinkText(data.findAnonymousTmpByShortId.link);
    }
  }, [data]);

  useEffect(() => {
    if (!debouncedSearchTerm) {
      return;
    }
    const updateNewTmp = () => {
      updateTmp({
        variables: {
          link: debouncedSearchTerm,
          id: data?.findAnonymousTmpByShortId._id,
        },
      });
    };
    updateNewTmp();
  }, [debouncedSearchTerm]);

  const copyToClipboard = async (link: string) => {
    if (navigator.clipboard) {
      try {
        await navigator.clipboard.writeText(link);
        toast.success(`copied: ${link}`);
      } catch (err) {
        console.error(err);
      }
    }
  };

  return (
    <CenteredLayout className="h-full flex flex-col justify-center space-y-4">
      <div className={tw`flex justify-between sm:justify-start space-x-4`}>
        <h1 className={tw`text-3xl sm:text-4-xl font-bold`}>
          Code : {shortId}
        </h1>
        <Button type="button" onClick={() => copyToClipboard(`${shortId}`)}>
          <Clipboard color="#fff" />
        </Button>
      </div>
      <div className={tw`flex space-x-4`}>
        <Input
          placeholder="insert link to share"
          onChange={(e) => setLinkText((e.target as any).value)}
          value={linkText}
        />
        <Button
          type="button"
          onClick={() => {
            refetch();
          }}
        >
          <RefreshCw color="#fff" />
        </Button>
      </div>
      <div className={tw`flex space-x-4`}>
        <Button onClick={() => copyToClipboard(linkText)}>
          <Clipboard color="#fff" />
        </Button>
        <a href={linkText} rel="noopener noreferrer nofollow" target="_blank">
          <Button>
            <LinkIcon color="#fff" />
          </Button>
        </a>
        <Button onClick={deleteSessionHandler} disabled={deleteLoading}>
          <Trash2 color="#fff" />
        </Button>
      </div>
      <ToastContainer
        position="bottom-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </CenteredLayout>
  );
};

export default Anonymous;
