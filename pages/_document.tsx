import Document, { Html, Head, Main, NextScript } from "next/document";
import { createElement } from "react";
import { setup } from "twind";
import { asyncVirtualSheet, getStyleTagProperties } from "twind/server";
import twindConfig from "../twind.config";

const sheet = asyncVirtualSheet();

setup({ ...twindConfig, sheet });

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    sheet.reset();

    const initialProps: any = await Document.getInitialProps(ctx);

    const { id, textContent } = getStyleTagProperties(sheet);

    const styleProps = {
      id,
      key: id,
      dangerouslySetInnerHTML: {
        __html: textContent,
      },
    };
    return {
      ...initialProps,
      styles: [...initialProps.styles, createElement("style", styleProps)],
    };
  }

  render() {
    return (
      <Html lang="en">
        <Head>
          <meta name="theme-color" content="#00D28B" />

          <meta name="application-name" content="Linked" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta
            name="apple-mobile-web-app-status-bar-style"
            content="default"
          />
          <meta name="apple-mobile-web-app-title" content="Linked" />
          <meta name="format-detection" content="telephone=no" />
          <meta name="mobile-web-app-capable" content="yes" />
          <meta name="msapplication-TileColor" content="#00D28B" />
          <meta name="msapplication-tap-highlight" content="no" />
          <meta name="theme-color" content="#00D28B" />
        </Head>
        <body>
          <Main />
          <NextScript />
          <script
            async
            src="https://ackee.ragz.dev/script.js"
            data-ackee-server="https://ackee.ragz.dev"
            data-ackee-domain-id="05081628-2528-4526-a8a2-52a965e1e832"
            data-ackee-opts='{"detailed": true}'
          />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
