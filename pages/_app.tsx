/* eslint-disable react/jsx-props-no-spreading */
import Head from "next/head";
import { ApolloProvider } from "@apollo/client";
import "../styles/globals.css";
import { setup } from "twind";
import { useApollo } from "../apollo/client";
import twindConfig from "../twind.config";

if (typeof window !== "undefined") {
  setup(twindConfig);
}

const MyApp: React.FC<any> = ({ Component, pageProps }) => {
  const apolloClient = useApollo(pageProps.initialApolloState);
  return (
    <>
      <Head>
        <title>Linked - Quickly share links across devices</title>
        <meta
          property="og:title"
          content="Linked - Quickly share links across devices"
          key="title"
        />

        <meta
          name="description"
          content="Quickly share links between your devices."
        />

        <link rel="icon" href="/assets/icons/favicon.ico" />
      </Head>

      <ApolloProvider client={apolloClient}>
        <Component {...pageProps} />
      </ApolloProvider>
    </>
  );
};

export default MyApp;
