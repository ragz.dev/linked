import { useState } from "react";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { useMutation } from "@apollo/client";
import { tw } from "twind";
import Input from "../components/input/input";
import Button from "../components/button/button";
import { LOGIN } from "../apollo/mutations";
import { setToken, setUser, isLoggedIn } from "../services/auth";
import CenteredLayout from "../components/centered-layout/centered-layout";

const Login: React.FC = () => {
  const router = useRouter();
  const [errorMessage, setErrorMessage] = useState("");
  const { handleSubmit, register, errors } = useForm<{
    email: string;
    password: string;
  }>();
  const [login, { loading, error, data }] = useMutation<{
    login: { token: string; account: { email: string; _id: string } };
  }>(LOGIN);

  if (isLoggedIn()) {
    router.push("/dashboard");
  }

  if (data?.login) {
    // Save token in cookies
    setToken(data.login.token);
    // Store user details
    setUser(data.login.account);
    // Redirect to the default private page
    router.push("/dashboard");
  }

  const onSubmit = handleSubmit(({ email, password }) => {
    if (errorMessage) setErrorMessage("");

    login({
      variables: {
        email,
        password,
      },
    });
  });

  return (
    <>
      <CenteredLayout className="h-full flex flex-col justify-center space-y-8">
        <h1 className={tw`text-4xl text-center font-bold`}>Log In</h1>

        <form onSubmit={onSubmit} className={tw`space-y-4`}>
          <div>
            <label htmlFor="email">Email</label>
            <Input
              id="email"
              type="email"
              name="email"
              ref={register({ required: "Email is required" })}
            />

            {errors.email && <span role="alert">{errors.email.message}</span>}
          </div>

          <div>
            <label htmlFor="password">Password</label>
            <Input
              id="password"
              type="password"
              name="password"
              ref={register({ required: "Password is required" })}
            />
            {errors.password && (
              <span role="alert">{errors.password.message}</span>
            )}
          </div>

          <div>
            <Button type="submit">Log in</Button>
          </div>
          {errorMessage && <p role="alert">{errorMessage}</p>}
        </form>
      </CenteredLayout>
    </>
  );
};

export default Login;
