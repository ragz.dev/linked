import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { useMutation } from "@apollo/client";
import { tw } from "twind";
import Input from "../components/input/input";
import Button from "../components/button/button";
import { REGISTER } from "../apollo/mutations";
import { setToken, setUser } from "../services/auth";
import CenteredLayout from "../components/centered-layout/centered-layout";

const Register: React.FC = () => {
  const router = useRouter();
  const { handleSubmit, register, watch, errors } = useForm<{
    email: string;
    password: string;
    password2: string;
  }>();

  const [registerUser, { loading, error, data }] = useMutation<{
    register: { token: string; account: { email: string; _id: string } };
  }>(REGISTER);

  if (data?.register) {
    // Save token in cookies
    setToken(data.register.token);
    // Store user details
    setUser(data.register.account);
    // Redirect to the default private page
    router.push("/dashboard");
  }

  const onSubmit = handleSubmit(async ({ email, password }) => {
    try {
      registerUser({
        variables: {
          email,
          password,
        },
      });
    } catch (regError) {
      throw new Error(regError);
    }
  });

  return (
    <CenteredLayout className="h-full flex flex-col justify-center space-y-8">
      <h1 className={tw`text-4xl text-center font-bold`}>Sign Up</h1>

      <form onSubmit={onSubmit} className={tw`space-y-4`}>
        <div>
          <label htmlFor="email">Email</label>
          <Input
            id="email"
            type="email"
            name="email"
            placeholder="e.g. john@example.com"
            ref={register({ required: "Email is required" })}
          />
          {errors.email && <span role="alert">{errors.email.message}</span>}
        </div>

        <div>
          <label htmlFor="password">Password</label>
          <Input
            id="password"
            type="password"
            name="password"
            placeholder="******"
            ref={register({ required: "Password is required" })}
          />
          {errors.password && (
            <span role="alert">{errors.password.message}</span>
          )}
        </div>

        <div>
          <label htmlFor="password2">Confirm Password</label>
          <Input
            id="password2"
            type="password"
            name="password2"
            placeholder="******"
            ref={register({
              validate: (value) =>
                value === watch("password") || "Passwords do not match",
            })}
          />
          {errors.password2 && (
            <span role="alert">{errors.password2.message}</span>
          )}
        </div>

        <div>
          <Button type="submit">Sign up</Button>
        </div>
        {error && <p role="alert">{error.message}</p>}
      </form>
    </CenteredLayout>
  );
};

export default Register;
