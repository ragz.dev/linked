import React, { useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { useMutation } from "@apollo/client";
import { nanoid } from "nanoid";
import { tw } from "twind";
import Button from "../components/button/button";
import Input from "../components/input/input";
import { CREATE_ANONYMOUS_TMP } from "../apollo/mutations";
import { isLoggedIn } from "../services/auth";

interface Props {
  token: string;
}

export const Home: React.FC<Props> = () => {
  const router = useRouter();
  const [shortId, setShortId] = useState("");

  const [createAnonymousTmp, { loading, error, data }] = useMutation<{
    createAnonymousTmp: any;
  }>(CREATE_ANONYMOUS_TMP);

  if (data?.createAnonymousTmp) {
    router.push(`/anonymous/${data.createAnonymousTmp.shortId}`);
  }

  const createAnonymousTmpHandler = () => {
    const newShortId = nanoid(6);
    createAnonymousTmp({ variables: { shortId: newShortId, link: "hello" } });
  };

  const changeShortId = (e) => {
    setShortId(e.target.value);
  };

  const joinAnonymousSession = () => {
    router.push(`/anonymous/${shortId}`);
  };

  return (
    <>
      <div
        className={tw`h-full max-w-lg mx-auto justify-center flex flex-col p-4 space-y-4`}
      >
        <div className={tw`flex space-x-4`}>
          {isLoggedIn() ? (
            <Link href="/dashboard">
              <Button>Dashboard</Button>
            </Link>
          ) : (
            <>
              <Link href="/login">
                <Button flexGrow={1}>Log in</Button>
              </Link>
              <Link href="/register">
                <Button flexGrow={1}>Register</Button>
              </Link>
            </>
          )}
        </div>
        <Button disabled={loading} onClick={createAnonymousTmpHandler}>
          Start an anonymous session
        </Button>
        <div className={tw`flex space-x-4`}>
          <Input
            placeholder="#shortId"
            onChange={changeShortId}
            className={tw`flex-grow-2`}
          />
          <Button onClick={joinAnonymousSession} disabled={!shortId}>
            Join
          </Button>
        </div>
      </div>
    </>
  );
};

export default Home;
