import { gql } from "@apollo/client";

export const LOGIN = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      account {
        _id
        email
      }
    }
  }
`;

export const REGISTER = gql`
  mutation register($email: String!, $password: String!) {
    register(email: $email, password: $password) {
      token
      account {
        _id
        email
      }
    }
  }
`;

export const LOGOUT = gql`
  mutation logout {
    logout
  }
`;

export const CREATE_BOOKMARK = gql`
  mutation createBookmark($link: String!, $profileID: ID!) {
    createBookmark(data: { link: $link, profile: { connect: $profileID } }) {
      link
    }
  }
`;

export const UPDATE_TMP = gql`
  mutation updateTmp($id: ID!, $link: String!) {
    updateTmp(id: $id, data: { link: $link }) {
      link
    }
  }
`;

export const UPDATE_ANONYMOUS_TMP = gql`
  mutation partialUpdateAnonymousTmp($id: ID!, $link: String!) {
    partialUpdateAnonymousTmp(id: $id, data: { link: $link }) {
      link
    }
  }
`;

export const CREATE_ANONYMOUS_TMP = gql`
  mutation createAnonymousTmp($shortId: String!, $link: String!) {
    createAnonymousTmp(data: { shortId: $shortId, link: $link }) {
      shortId
    }
  }
`;

export const DELETE_ANONYMOUS_TMP = gql`
  mutation deleteAnonymousTmp($id: ID!) {
    deleteAnonymousTmp(id: $id) {
      _id
    }
  }
`;
