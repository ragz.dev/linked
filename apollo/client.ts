import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
  NormalizedCacheObject,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { useMemo } from "react";
import { getToken } from "../services/auth";

let apolloClient: ApolloClient<NormalizedCacheObject>;

const endpoint = "https://graphql.fauna.com/graphql";

const httpLink = createHttpLink({
  uri: endpoint,
  // Additional options
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token =
    getToken() ||
    process.env.NEXT_PUBLIC_FAUNA_GUEST_SECRET ||
    process.env.FAUNA_GUEST_SECRET;
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      "X-Schema-Preview": "partial-update-mutation",
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});

export const createApolloClient = (): ApolloClient<NormalizedCacheObject> =>
  new ApolloClient({
    ssrMode: typeof window === "undefined",
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
  });

export function initializeApollo(
  initialState = null
): ApolloClient<NormalizedCacheObject> {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const _apolloClient = apolloClient ?? createApolloClient();

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // gets hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract();
    // Restore the cache using the data passed from getStaticProps/getServerSideProps
    // combined with the existing cached data
    _apolloClient.cache.restore({ ...existingCache, ...initialState });
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === "undefined") return _apolloClient;
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;
  return _apolloClient;
}

export function useApollo(initialState): any {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);
  return store;
}
