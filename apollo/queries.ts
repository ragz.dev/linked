import { gql } from "@apollo/client";

export const FIND_ACCOUNT_BY_ID = gql`
  query findAccountById($id: ID!) {
    findAccountByID(id: $id) {
      _id
      email
      profile {
        _id
        tmp {
          link
          _id
        }
        bookmarks {
          data {
            _id
            link
          }
        }
      }
    }
  }
`;

export const FIND_ACCOUNT_BY_EMAIL = gql`
  query findAccountByEmail($email: String!) {
    findAccountByEmail(email: $email) {
      _id
      email
      profile {
        _id
        bookmarks {
          data {
            _id
            link
          }
        }
      }
    }
  }
`;

export const FIND_ANONYMOUS_TMP_BY_SHORTID = gql`
  query FindAnonymousTmpByShortId($shortId: String!) {
    findAnonymousTmpByShortId(shortId: $shortId) {
      _id
      link
    }
  }
`;
