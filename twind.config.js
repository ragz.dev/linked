module.exports = {
  theme: {
    gradientColorStops: (theme) => ({
      ...theme("colors"),

      primary: "#D7FFFE",

      background: "#FAFAFA",
    }),
    extend: {
      colors: {
        primary: "#D7FFFE",
        background: "#FAFAFA",
        dark: "#161E2E",
      },
      backgroundImage: {
        "bg-gradient-to-95":
          "linear-gradient(95deg, var(--tw-gradient-stops));",
      },
      minHeight: {
        "screen-50": "50vh",
        "screen-60": "60vh",
      },
      fontFamily: {
        sans:
          "Inter, system-ui,-apple-system, /* Firefox supports this but not yet `system-ui` */'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji'",
      },
    },
  },
};
