import Cookies from "js-cookie";
import isBrowser from "../utils/is-browser";

const userKey = "user_data";

const tokenKey = "token";

interface User {
  email: string;
  _id: string;
}

export const setToken = (token: string): string => Cookies.set(tokenKey, token);

export const getToken = (): string => Cookies.get(tokenKey);

export const getUser = (): User =>
  isBrowser() && window.localStorage.getItem(userKey)
    ? JSON.parse(window.localStorage.getItem(userKey))
    : {};

export const setUser = (user: User): void =>
  window.localStorage.setItem(userKey, JSON.stringify(user));

export const isLoggedIn = (): boolean => {
  const user = getUser();
  return !!user.email;
};

export const cleanUp = (): void => {
  Cookies.remove(tokenKey);
  window.localStorage.removeItem(userKey);
};
